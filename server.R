#
# This is the server logic of a Shiny web application. You can run the 
# application by clicking 'Run App' above.
#
# Find out more about building applications with Shiny here:
# 
#    http://shiny.rstudio.com/
#

library(shiny)
library(rhandsontable)

create.HIS <-function() {
  HIS <-  data.frame(matrix(rnorm(0), ncol = 3, nrow = 23))
  col_names <- c("2016","2017","2018")
  row_names <- c("Revenue", "COGS", "Gross Margin", "General and Administrative", "Selling and Marketing", "Research and Development",
                 "Total Operating Expense", "EBITDA", 'Depreciation', 'Amortization of Capitalized Financing Fees', 
                 'Goodwill Impairment', 'IP&C Amortization', 'Allowance for Doubtful Accounts', 'Inventory Reserve', 'Other Non-cash Items',  
                 'EBIT', 'Interest Expense', 'Interest (Income)', 'Income(Loss) before Income Taxes', 'Deferred Taxes', 'Current Taxes',
                 'Total Income Tax Provision', 'Net Income(Loss)' )
  colnames(HIS) <- col_names
  rownames(HIS) <- row_names
  return(HIS)
}

create.HBS <-function() {
  HBS <-  data.frame(matrix(rnorm(0), ncol = 3, nrow = 69))
  col_names <- c("2016", "2017", "2018")
  row_names <- c('Cash', 'Accounts Receivables',
                 'Allowance for Doubtful Accounts','Net Accounts Receivables',
                 'Inventories', 'Inventory Reserves','Net Inventories', 'Prepaid Assets',
                 'Other Current Assets', 'Total Current Assets', 'Property, Plant and Equipment',
                 'Accumulated Depreciation', 'Net PP&E', 'Capitalized Financing Fees',
                 'Accummulated Amortization for CFS', 'Net Capitalized Financing Fees',
                 'Goodwill', 'Impairment', 'Net Goodwill',
                 'Patents and Copyrights','Accumulated Amortization for P&C', 'Net IP',
                 'Other Intangible Assets', 'Accumulated Amortization for Others',
                 'Total Other Intangibles', 'Net Long Term Assets',
                 'Total Assets', 'Accounts Payables', 'Accrued Liabilities', 'Revolver',
                 'Income Taxes Payable', 'Current Portion of Deferred Tax Liabilities',
                 'Other Current Liabilities', 'Current Portion of Long Term Funded Debt',
                 'Current Portion of Capitalized Leases', 
                 'Common Dividend Payable', 'Preferred Dividend Payable','Total Current Liabilities',
                 'Senior Bank Debt - ABL', 'Senior Bank Debt - Mortgage', 'Senior Bank Debt - CF', 
                 'Senior Subordinated Debt', 'Junior Subordinated Debt', 'Bridge Loan', 'Mezzanine Debt', 'High Yield Bonds',
                 'Convertible Debt', 'Debentures', 'Convertible Debentures', 'Seller Notes', 
                 'Long Term Capital Leases', 'Deferred Tax Liability - Long Term', 'Other Long Term Liabilities', 
                 'Total Long Term Debt', 'Total Liabilities', 'Common  Stock, Par + Paid-in',
                 'Common Stock Class A, Par + Paid-in', 'Common Stock Class B, Par + Paid-in',
                 'Convertible Preferred Stock A, Par + Paid-in', 'Convertible Preferred Stock B, Par + Paid-in', 
                 'Convertible Preferred Stock C, Par + Paid-in', 'Convertible Preferred Stock D, Par + Paid-in', 
                 'Convertible Preferred Stock E, Par + Paid-in', 'Current Period Net Income(Loss)',
                 'Retained Earnings', 'Common Dividend', 'Preferred Dividend', 'Total Equity', 
                 'Total Liabilities and Equity')
  colnames(HBS) <- col_names
  rownames(HBS) <- row_names
  return(HBS)
}

create.HCFS <- function() {
  HCFS <-  data.frame(matrix(rnorm(0), ncol = 3, nrow = 60))
  col_names <- c("2016", "2017", "2018")
  row_names <- c('Net Income(Loss)', 'Depreciation', 'Amortization of Capitalized Financing Fees','Goodwill Impairment',
                 'Intangible Amortization', 'Change in Allowance for Doubtful Accounts',
                 'Change in Inventory Reserve', 'Changes in Other Intangibles and Non-cash Items',
                 'Cash from Operations', 'Accounts Receivables', 'Inventories', 
                 'Prepaid Assets', 'Other Current Assets', 'Accounts Payables', 'Accrued Liabilities', 
                 'Income Taxes Payable', 'Current Portion of Deferred Tax Liabilities', 'Other Current Liabilities', 
                 'Current Portion of Long Term Funded Debt', 'Current Portion of Capitalized Leases', 
                 'Common Dividend Payable', 'Preferred Dividend Payable', 'Cash from Working Capital', 
                 'Capital Expenditures', 'Capitalized Financing Fees', 'Goodwill', 'Patents and Copyrights', 
                 'Other Intangible Assets', 'Cash from Investing Activities', 'Revolver', 'Senior Bank Debt - ABL', 
                 'Senior Bank Debt - Mortgage', 'Senior Bank Debt - CF', 'Senior Subordinated Debt', 
                 'Junior Subordinated Debt', 'Bridge Loan', 'Mezzanine Debt', 'High Yield Bonds','Convertible Debt', 'Debentures', 
                 'Convertible Debentures', 'Seller Notes', 'Long Term Capital Leases', 'Deferred Tax Liability - Long Term', 
                 'Other Long Term Liabilities', 'Common  Stock, Par + Paid-in', 'Common Stock Class A, Par + Paid-in', 
                 'Common Stock Class B, Par + Paid-in', 'Convertible Preferred Stock A, Par + Paid-in', 
                 'Convertible Preferred Stock B, Par + Paid-in', 'Convertible Preferred Stock C, Par + Paid-in', 
                 'Convertible Preferred Stock D, Par + Paid-in', 'Convertible Preferred Stock E, Par + Paid-in', 
                 'Common Dividend', 'Preferred Dividend', '(Repay)Borrow Senior Term (Secured)', 
                 'Cash from Financing Activities', 'Change in Cash', 'Beginning Cash', 'Ending Cash' )
  colnames(HCFS) <- col_names
  rownames(HCFS) <- row_names
  return(HCFS)
}


# Define server logic required to draw a histogram
shinyServer(function(input, output) {
  
  v = reactiveValues()
   
  observe({input$HIS
    if (!is.null(input$HIS)) {
      v$HIS <- hot_to_r(input$HIS)
    } else {
      v$HIS <-create.HIS()
       
      
    }
  })
  
  
  
  output$HIS <- renderRHandsontable({
    rhandsontable(v$HIS, rowHeaderWidth = 350) %>%
      hot_cols(colWidths = 100) %>%
      hot_rows(rowHeights = 30)
  })
  
  #------------------------------
  
  observe({input$HBS
    if (!is.null(input$HBS)) {
      v$HBS <- hot_to_r(input$HBS)
    } else {
      v$HBS <-create.HBS()
      
      
    }
  })
  
  
  
  output$HBS <- renderRHandsontable({
    rhandsontable(v$HBS, rowHeaderWidth = 350) %>%
      hot_cols(colWidths = 100) %>%
      hot_rows(rowHeights = 30)
  })
  
  #-----------------------
  
  observe({input$HCFS
    if (!is.null(input$HCFS)) {
      v$HCFS <- hot_to_r(input$HCFS)
    } else {
      v$HCFS <-create.HCFS()
      
      
    }
  })
  
  
  
  output$HCFS <- renderRHandsontable({
    rhandsontable(v$HCFS, rowHeaderWidth = 350) %>%
      hot_cols(colWidths = 100) %>%
      hot_rows(rowHeights = 30)
  })
  
})
